import Vue from 'vue'
import Router from 'vue-router'

const routerOptions = [
  { path: '*', component: 'NotFound' },
  { path: '/', component: 'Home' },
  { path: '/about', component: 'About' },
  { path: '/login', component: 'Login' },
  { path: '/signup', component: 'SignUp' }
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  routes,
  mode: 'history'
})
